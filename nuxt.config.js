import VuetifyLoaderPlugin from 'vuetify-loader/lib/plugin'
// import pkg from './package'

export default {
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: 'Human Beyond Labs - AI Software Development & Digital Art Studio',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'We explore the intersection of cutting edge technology and human expression; combining Web Technologies, Artificial Intelligence, and Digital Art.'
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content:
          'ai, machine learning, digital art, generative art, creative coding, workshops, amsterdam, interactive instalation, creative agency, creative studio, software development, data science, '
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Global CSS
   */
  css: ['~/assets/style/app.styl', '~/assets/style/base.scss'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['@/plugins/vuetify'], // , '@/plugins/particles'

  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    // 'nuxt-cookie-control',
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-139203435-1'
      }
    ]
  ],

  /*
   ** Build configuration
   */
  build: {
    transpile: ['vuetify/lib'],
    plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      stylus: {
        import: ['~assets/style/variables.styl']
      }
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
