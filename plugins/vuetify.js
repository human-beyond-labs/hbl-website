import Vue from 'vue'
import Vuetify from 'vuetify/lib'
// import colors from 'vuetify/es5/util/colors'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify, {
  options: {
    customProperties: true
  },
  iconfont: 'fa',
  theme: {
    primary: '#E91E63',
    secondary: '#4C00B4',
    accent: '#6200EA',
    error: '#FF1744',
    warning: '#FFC400',
    info: '#2196f3',
    success: '#00BFA5'
  }
})

// Vue.use(Vuetify, {
//   theme: {
//     primary: colors.blue.darken2,
//     accent: colors.grey.darken3,
//     secondary: colors.amber.darken3,
//     info: colors.teal.lighten1,
//     warning: colors.amber.base,
//     error: colors.deepOrange.accent4,
//     success: colors.green.accent3
//   }
// })
