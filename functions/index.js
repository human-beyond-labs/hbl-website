const functions = require('firebase-functions')
const { IncomingWebhook } = require('@slack/client')

// Slack webhook client used to post messages to a channel.
const webhook = new IncomingWebhook(functions.config().slack.webhook)

// Posts a comment to a slack channel with a random quote and contact
// information when a message is submitted from the contact form to firestore
exports.sendToSlack = functions.firestore
  .document('messages/{messageId}')
  .onCreate((snap, context) => {
    // Get an object representing the document
    // e.g. {'name': 'Marie', 'message': 'tha message...'}
    const data = snap.data()

    const message = `
        A new message from the contact from
        Name: ${data.name}
        Email: ${data.emailaddress}
        Message: ${data.message}`

    // Send simple text to the webhook channel
    webhook.send(message, (err, res) => {
      if (err) {
        console.log('Error:', err)
      }
    })

    // Promise method
    // return new Promise((resolve, reject) => {
    //   webhook.send(message, (err, res) => {
    //     if (err) {
    //       reject(err);
    //       return;
    //     }
    //     resolve();
    //   });
    // });
  })
